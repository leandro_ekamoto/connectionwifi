package br.com.cpfl.connectionwifi;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * <p> Class Connection Wifi <p/>
 *
 * @author Leandro Shindi Ekamoto, Lucas Amaral
 * @version 1.0 12/08/15.
 */
public class ConnectionWifi extends AsyncTask<Bundle, Bundle, Void> {

    private static WifiManager wifi;
    private final String TAGLOG = "NastekWifiLog";
    private Context context;
    private int contError;
    private TextView textView;
    private boolean test;

    /**
     * <p> Construct </p>
     */
    public ConnectionWifi(Context context, TextView textView) {

        this.wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        this.context = context;
        this.contError = 0;
        this.textView = textView;
        this.test = true;
    }

    /**
     * <p> Set Test </p>
     *
     * */
    public void setTest(boolean test) {

        this.test = test;
    }

    /**
     * <p> Process Background </p>
     */
    @Override
    protected Void doInBackground(Bundle... params) {

        Log.e(TAGLOG, "Starting Connection...");

        int cont = 0;
        int id;
        boolean conectado;

        while (this.test) {

            activateWifi();
            removeNetworks();
            List<ScanResult> listScanResult = getListScanResults();
            WifiConfiguration wifiConfiguration;

            if (cont % 2 == 0) {

                // TODO
                // No lugar das duas linhas abaixo adicione sua rotina para conectar com proxy
                ScanResult sr = getScanResult(listScanResult, params[0].getString("network_1"));
                wifiConfiguration = getWifiConfiguration(sr, params[0].getString("password"));
            } else {

                ScanResult sr = getScanResult(listScanResult, params[0].getString("network_2"));
                wifiConfiguration = getWifiConfigurationDL(sr);
            }

            id = addNetWork(wifiConfiguration);
            saveConfiguration();

            conectado = connect(id);

            if (conectado) {

                Log.e(TAGLOG, "Connect OK " + wifiConfiguration.SSID);

                Bundle bundle = new Bundle();
                bundle.putString("SSID", wifiConfiguration.SSID);

                publishProgress(bundle);
            } else {

                Log.e(TAGLOG, "Connect ERRO");
                this.contError++;

                Bundle bundle = new Bundle();
                bundle.putString("SSID", "");
                publishProgress(bundle);
            }

            cont++;
            sleepProcess(10000);
            deactivateWifi();
            sleepProcess(10000);
        }
        return null;
    }

    /**
     * <p> Remove </p>
     */
    public void removeNetworks() {

        List<WifiConfiguration> configs = wifi.getConfiguredNetworks();
        Log.e(TAGLOG, "Remove SSID:");

        for (WifiConfiguration config : configs) {

            Log.e(TAGLOG, "" + config.SSID);
            wifi.removeNetwork(config.networkId);
        }
    }

    @Override
    protected void onProgressUpdate(Bundle... values) {

        if (!values[0].get("SSID").equals("")) {

            Toast.makeText(context, "SSID: " + values[0].get("SSID"), Toast.LENGTH_LONG).show();
        } else {

            Toast.makeText(context, "SSID Empty", Toast.LENGTH_LONG).show();
        }

        textView.setText("Falha ao conectar Wifi: " + getContError());
    }

    /**
     * <p> Activate Wifi </p>
     *
     * @return {@code true} if the operation succeeded
     */
    public boolean activateWifi() {

        if (wifi.isWifiEnabled()) {

            Log.e(TAGLOG, "Wifi Already On...");
            return true;
        }

        boolean ativado = wifi.setWifiEnabled(true);

        while (!wifi.isWifiEnabled()) {

            Log.e(TAGLOG, "Loading...Activating Wifi\n");
            sleepProcess(2000);
        }

        Log.e("WifiHisamoto", "Wifi Activated");
        return ativado;
    }

    /**
     * <p> Deactivate Wifi </p>
     *
     * */
    public boolean deactivateWifi() {

        if (!wifi.isWifiEnabled()) {

            Log.e(TAGLOG, "Wifi Disable");
            return true;
        }

        boolean desativado = wifi.setWifiEnabled(false);
        while (wifi.isWifiEnabled()) {

            Log.e(TAGLOG, "Loading...Disabling Wifi");
            sleepProcess(2000);
        }

        Log.e(TAGLOG, "Wifi Disable");
        return desativado;
    }

    /**
     * <p> Connect Wifi <p/>
     *
     * @param netId The ID of the newly created network description. This is used in
     *              other operations to specified the network to be acted upon.
     *              Returns {@code -1} on failure.
     * @return {@code true} if the operation succeeded
     */
    public boolean connect(int netId) {

        if (wifi.enableNetwork(netId, true)) {

            Log.e(TAGLOG, "Trying to Connect");
            return wifi.reconnect();
        }

        Log.e(TAGLOG, "Failure to Connect" + netId);
        return false;
    }

    /**
     * <p> Search for available networks <p/>
     *
     * @return List ScanResult
     */
    public List<ScanResult> getListScanResults() {

        wifi.startScan();
        List<ScanResult> scans = wifi.getScanResults();

        while (scans.size() == 0) {

            try {

                Log.e(TAGLOG, "Loading...Search SSID");
                scans = wifi.getScanResults();
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Log.e(TAGLOG, "List SSID ScanResult:");
        if (scans != null) {

            for (ScanResult scan : scans) {

                Log.e(TAGLOG, "" + scan.SSID);
            }
        }
        return scans;
    }

    /**
     * <p> Return ScanResult <p/>
     *
     * @param scans List networks
     * @param ssid  ssid
     * @return ScanResult
     */
    public ScanResult getScanResult(List<ScanResult> scans, String ssid) {

        ScanResult s = null;
        if (scans != null) {

            for (ScanResult scan : scans) {

                if (scan.SSID.contains(ssid)) {

                    s = scan;
                    break;
                }
            }
        }

        return s;
    }

    /**
     * Return Configuration Wifi
     *
     * @param sr ScanResult
     * @return WifiConfiguration
     */
    public WifiConfiguration getWifiConfiguration(ScanResult sr, String password) {

        if (sr == null)
            return null;

        WifiConfiguration wc = new WifiConfiguration();

        // O conjunto de protocolos de autenticação suportados por esta configuração
        // Padrões para a selecção automática.
        wc.allowedAuthAlgorithms.clear();

        // O conjunto de cifras de grupo suportados por esta configuração
        // O padrão é CCMP TKIP WEP104 WEP40.
        wc.allowedGroupCiphers.clear();

        // O conjunto de cifras de pares para WPA suportados por esta configuração
        // O padrão é CCMP TKIP.
        wc.allowedPairwiseCiphers.clear();

        // O conjunto de protocolos de segurança suportados por esta configuração.
        // O padrão é WPA RSN.
        wc.allowedProtocols.clear();

        // O conjunto de protocolos de gestão chave suportados por esta configuração.
        // O padrão é WPA-PSK WPA-EAP.
        wc.allowedKeyManagement.clear();

        wc.SSID = "\"".concat(sr.SSID).concat("\"");
        wc.preSharedKey = "\"".concat(password).concat("\"");

        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        wc.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);

        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);

        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        wc.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);

        wc.allowedProtocols.set(WifiConfiguration.Protocol.RSN); // For WPA2
        wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA); // For WPA

        wc.priority = 0;
        //wc.hiddenSSID = true;
        wc.status = WifiConfiguration.Status.ENABLED;

        return wc;
    }

    /**
     * <p> Clear </p>
     *
     * @param wc WifiConfiguration
     *
     * */
    public void clearWifiConfiguration(WifiConfiguration wc) {

        // O conjunto de protocolos de autenticação suportados por esta configuração
        // Padrões para a selecção automática.
        wc.allowedAuthAlgorithms.clear();

        // O conjunto de cifras de grupo suportados por esta configuração
        // O padrão é CCMP TKIP WEP104 WEP40.
        wc.allowedGroupCiphers.clear();

        // O conjunto de cifras de pares para WPA suportados por esta configuração
        // O padrão é CCMP TKIP.
        wc.allowedPairwiseCiphers.clear();

        // O conjunto de protocolos de segurança suportados por esta configuração.
        // O padrão é WPA RSN.
        wc.allowedProtocols.clear();

        // O conjunto de protocolos de gestão chave suportados por esta configuração.
        // O padrão é WPA-PSK WPA-EAP.
        wc.allowedKeyManagement.clear();
    }

    /**
     * <p> Return Configuration Wifi DL <p/>
     *
     * @param sr ScanResult
     * @return WifiConfiguration
     */
    public WifiConfiguration getWifiConfigurationDL(ScanResult sr) {

        if (sr == null)
            return null;

        WifiConfiguration wc = new WifiConfiguration();
        clearWifiConfiguration(wc);

        wc.SSID = "\"" + sr.SSID + "\"";
        wc.BSSID = sr.BSSID;
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        wc.hiddenSSID = false;

        return wc;
    }

    /**
     * Add Network
     *
     * @param wc WifiConfiguration
     * @return id network
     */
    public int addNetWork(WifiConfiguration wc) {

        int ret = wifi.addNetwork(wc);

        return ret;
    }

    /**
     * Save
     */
    public void saveConfiguration() {

        wifi.saveConfiguration();
    }

    /**
     * Sleep Process
     * </p>
     *
     * @param i Time
     */
    private void sleepProcess(long i) {

        try {

            Thread.sleep(i);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
    }

    /**
     * ContError
     *
     * */
    public int getContError() {

        return this.contError;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
    }

    @Override
    protected void onCancelled(Void aVoid) {
        super.onCancelled(aVoid);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
