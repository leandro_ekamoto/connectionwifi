package br.com.cpfl.connectionwifi;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TextView textView = (TextView) findViewById(R.id.qtd_error);
        String networkDLWifi = "";
        String networkWifi = "";
        String passwordWifi = "";

        Bundle bundle = new Bundle();
        bundle.putString("password", passwordWifi);
        bundle.putString("network_1", networkWifi);
        bundle.putString("network_2", networkDLWifi);

        ConnectionWifi connectionWifi = new ConnectionWifi(getApplicationContext(), textView);
        connectionWifi.execute(bundle);
    }
}
